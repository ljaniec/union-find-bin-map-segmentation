function [] = setValue(p, value)
%SETVALUE Sets given value on all leafs below the starting leaf p and
%         ending at its root, but excluding the root.
%   Inputs:
%    p          the index of a vertex
%    value      set value
%    uglobal    a global array recording relations of adjacency
%         
%   Outputs:
%    uglobal    a modified global array recording relations of adjacency

    global uglobal

    while uglobal(p) > 0
       l = uglobal(p);
       uglobal(p) = value;
       p = l;
    end
end

