function nclass = equiClass()

%    This function counts the number of equivalence classes, and labels 
%    each equivalence class with its ordinal number. 
%
%    Input
%    uglobal    a global array recording the relations, after a call to 
%               adjacencies()
%
%    Output
%    uglobal    a global array enumerating the equivalence classes
%    nclass     the number of equivalence classes

    global uglobal
    nclass = 0; % number of equivalence classes in uglobal
    
    for i = 1:numel(uglobal) % zliczanie klas równoważności
        if (uglobal(i) < 0)
            nclass = nclass + 1;
            uglobal(i) = (-1)*nclass; % update wartości w korzeniu numerem klasy
        end
    end

    for i = 1:numel(uglobal) % numerowanie liści
        if (uglobal(i) > 0)
            value = uglobal(getRootOf(i));
            setValue(i,value);
        end
    end
    uglobal = (-1)*uglobal;
end
