function [] = updatePair(p, q)
%UPDATATEPAIR Update relations of one pair of vertices.
%
%    Input
%    p, q       the indices of vertices
%    uglobal    a global array recording relations
%
%    Output
%    uglobal    a global array recording relations

    global uglobal
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % finding the roots
    rp = getRootOf(p);
    rq = getRootOf(q);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % if the two trees are distinct, merging them
    if (rp ~= rq)
        % the tree with the root rp is larger than the tree with the root rq
        if (-1)*uglobal(rp) < (-1)*uglobal(rq) % because the values in roots are with minus
            % updating the size of the bigger tree 
            uglobal(rq) = uglobal(rp) + uglobal(rq);
            % attaching rp (the smaller tree) to rq (the bigger tree)
            uglobal(rp) = rq;
            % linking all leaves of p and q to the root rq (by means of setValue)
            setValue(p, rq);
            setValue(q, rq);
        else % the tree with the root rp is smaller than the tree with the root rq
            % updating the size of the bigger tree 
            uglobal(rp) = uglobal(rp) + uglobal(rq);
            % attaching rq (the smaller tree) to rp (the bigger tree)
            uglobal(rq) = rp;
            % linking all leaves of p and q to the root rp (by means of setValue)
            setValue(p, rp);
            setValue(q, rp);
        end
    else % linking all leafs to the root also when rp == rq
            setValue(p, rp);
            setValue(q, rp);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
