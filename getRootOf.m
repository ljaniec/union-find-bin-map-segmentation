function r = getRootOf(p)
% GETROOTOF Finds the root of a vertex given a global array recording relations. 
%
%   Inputs:
%    p          the index of a vertex
%    uglobal    a global array recording relations
%
%   Outputs:
%    r          the index of the root of the vertex
global uglobal
r = p;
while uglobal(r) > 0
    r = uglobal(r);
end


