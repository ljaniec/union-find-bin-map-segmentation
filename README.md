on # Union-find-based binary map segmentation

Algorithm for the segmentation of binary maps, based on the union-find approach.

Main script: algUnionFind.m (Matlab R2019b)

