function nclass = equiClass()
% EQUICLASS Zlicza składowe spójności i oznacza każdą z ich składowych 
%           jej liczbą porządkową.
%
%    Wejście
%    uglobal    globalna macierz relacji przylegania, po wywołaniu 
%               adjacencies()
%
%    Wyjście
%    uglobal    globalna macierz relacji przylegania z oznaczonymi składowymi
%    nclass     liczba składowych spójności w obrazie
    global uglobal
    nclass = 0;
    for i = 1:numel(uglobal) % zliczanie klas równoważności
        if (uglobal(i) < 0)
            nclass = nclass + 1;
            uglobal(i) = (-1)*nclass; % aktualizacja wartości w korzeniu 
                                      %  numerem klasy
        end
    end

    for i = 1:numel(uglobal) % numerowanie liści
        if (uglobal(i) > 0)
            value = uglobal(getRootOf(i));
            setValue(i,value);
        end
    end
    uglobal = (-1)*uglobal;
end
