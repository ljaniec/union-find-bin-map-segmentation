function r = getRootOf(p)
% GETROOTOF Wyszukuje korzeń danego wierzchołka z podanej 
%	    globalnej macierzy relacji.
%
%   Wejścia:
%    p          indeks danego wierzchołka w macierzy uglobal
%    uglobal    globalna macierz relacji przylegania
%
%   Wyjście:
%    r          indeks korzenia drzewa z danym wierzchołkiem 
%				w macierzy uglobal
    global uglobal
    r = p;
    while uglobal(r) > 0
    r = uglobal(r);
    end
end
