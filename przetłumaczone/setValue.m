function [] = setValue(p, value)
%SETVALUE Wstawia podaną wartosć na wszystkich wierzchołkach 
%       drzewa poniżej danego wierzchołka p, z nim włącznie, 
%       do jego korzenia, ale z wyłączeniem korzenia.
%
%   Wejścia:
%    p          indeks wierzchołka w macierzy uglobal
%    value      wstawiana wartość
%    uglobal    globalna macierz relacji przylegania
%         
%   Wyjście:
%    uglobal    zmodyfikowana globalna macierz relacji przylegania
    global uglobal
    while uglobal(p) > 0
       l = uglobal(p);
       uglobal(p) = value;
       p = l;
    end
end

