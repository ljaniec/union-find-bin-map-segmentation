function [] = printMap(name)
% PRINTMAP Wyświetla mapę i zapisuje ją pod daną nazwą
%   Wejście
%   uglobal     globalna macierz relacji przylegania, po
%               wywołaniu funkcji adjacencies()
%   name        nazwa pliku wyjściowego, np. 'map1.png'
global uglobal
imagesc(uglobal)
colormap(jet)
set(gca,'xtick',[])
set(gca,'ytick',[])
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
saveas(gcf, name)
end

