function [binaryMap] = loadMap(map)
%LOADMAP Load given map in a form given in the Sturtevant's set of map.
%   binaryM = loadMap(ExMap) load map of @ and . as binary Map
%% Legend for map:
% . - passable terrain
% G - passable terrain
% @ - out of bounds
% O - out of bounds
% T - trees (unpassable)
% S - swamp (passable from regular terrain) -> passable
% W - water (traversable, but not passable from terrain) -> unpassable

% LOADMAP Wczytuje mapę ze zbioru \cite[sturtevant2012benchmarks] 
%         jako mapę binarną.
% 
% Przykład użycia: 
% binMap = loadMap('street-map/Berlin_0_1024.map') 
%
% Konwertuje w mapach ., @ oraz S jako jedynki, resztę jako zera.
%% Legenda map:
% . - teren przejezdny (ziemia)
% G - teren przejezdny 
% @ - teren nieprzejezdny (budynki)
% O - teren nieprzejezdny
% T - drzewa (nieprzejezdne)
% S - bagno (przejezdne)
% W - woda (nieprzejezdne)
fileID = fopen(map, 'r');
fgetl(fileID); % ignoruje 'type octile' na początku pliku
height = sscanf(fgetl(fileID), 'height %d'); % kolumny numerowane od 0
width = sscanf(fgetl(fileID), 'width %d'); % kolumny numerowane od 0
fgetl(fileID); % ignoruje 'map' poniżej pliku
passable = ['.', 'G', 'S']; % przejezdne
%unpassable = ['@', 'O', 'T', 'W']; % pozostałe
binaryMap = zeros(height, width); % wymiary mapy
for i = 1:height
    tmpLine = fgetl(fileID);
    for j = 1:width
        tmpChar = tmpLine(j);
        if strcmp(tmpChar, passable(1))||...
           strcmp(tmpChar, passable(2))||...
           strcmp(tmpChar, passable(3))
            binaryMap(i,j) = 0;
        else
            binaryMap(i,j) = 1;
        end
    end
end
fclose(fileID);
