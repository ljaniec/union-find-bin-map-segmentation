% Main script of implementation for the binary maps segmentation
% in optimized way, using the union-find algorithm

global uglobal;

uglobal = [-1, 3, 1, -2, 6, 4];

r1 = getRootOf(1)
r2 = getRootOf(2)
r3 = getRootOf(3)
r4 = getRootOf(4)
r5 = getRootOf(5)
r6 = getRootOf(6)
%%
binMap = loadMap('street-map/Berlin_0_256.map');
%% setValue
uglobal = [-1, 3, 1, -2, 6, 4, 2, 2]
setValue(2, 1);
uglobal

%%
% test for updatePair
uglobal = [-1, -1, -1, -1, -1, 5, 5, 7]
updatePair(7,5)
uglobal
%%
% test for updatePair

uglobal = [3, 1, -5, 3, 3, 9, 6, 9, -4, -1]
updatePair(9,3)
uglobal
updatePair(3,7) % pod przykład
uglobal
%%
% test for updatePair
fprintf("-------------------------------------------\n NEW \n");
uglobal = [2, -5, 2, 3, -4, 5, 5, 6, 4]
% rUP1 = getRootOf(1)
% rUP2 = getRootOf(2)
% rUP3 = getRootOf(3)
% rUP4 = getRootOf(4) 
% rUP5 = getRootOf(5)
% rUP6 = getRootOf(6)
% rUP7 = getRootOf(7)
% rUP8 = getRootOf(8)
% rUP9 = getRootOf(9)
%updatePair(2,8)
updatePair(8,2)
uglobal
%%
% equivalence and adjacencies - test 1
binMap = [1 1 0 1 1;0 0 0 1 0; 1  1  0 1 1];
adjacencies(binMap);
uglobal
nclass1 = equiClass()
uglobal
%%
% equivalence and adjacencies - test 2
uglobal
binMap = loadMap('street-map/Berlin_0_256.map');
adjacencies(binMap);
uglobal
nclass2 = equiClass()
uglobal
%%
% equivalence and adjacencies - test 3
uglobal
binM = loadMap('street-map/Berlin_0_1024.map');
adjacencies(binM);
uglobal
nclass3 = equiClass()
uglobal
%%
% printing of the map - test 1
binMap = [0 0 0 0 0 0 0; 0 1 1 0 1 1 0; 0 0 0 0 0 1 0; 0 1  1  0 1 1 0; 0 0 0 0 0 0 0]
adjacencies(binMap);
uglobal
nclass1 = equiClass() % dla przetworzenia mapy
uglobal
printMap('ex1.png')
%%
% printing of the map - test 2
binMap = loadMap('street-map/Berlin_0_1024.map');
adjacencies(binMap);
%uglobal
nclass1 = equiClass() % dla przetworzenia mapy
%uglobal
printMap('ex2.png')
%%
% W-13 map
binMap = ...
   [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
    0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 1 1 1 0 0;
    0 1 0 0 1 0 0 1 0 0 1 1 0 0 1 1 0 1 1 0;
    0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 0 1 0;
    0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 1 1 0;
    0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 1 1 0 0;
    0 1 0 0 1 0 0 1 0 0 0 1 0 0 0 0 0 1 1 0;
    0 1 0 1 1 1 0 1 0 0 0 1 0 0 0 0 0 0 1 0;
    0 1 1 1 0 1 1 1 0 0 0 1 0 0 1 1 0 1 1 0;
    0 1 0 0 0 0 0 1 0 0 1 1 1 0 0 1 1 1 0 0;
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
adjacencies(binMap);
%uglobal
nclass1 = equiClass(); % dla przetworzenia mapy
%uglobal
printMap('W13.png')
%%
% test for different maps of various sizes (256, 512, 1024)
maps = [
    "street-map/Berlin_0_256.map", "street-map/Berlin_0_512.map", "street-map/Berlin_0_1024.map",...
    "street-map/Berlin_1_256.map", "street-map/Berlin_1_512.map", "street-map/Berlin_1_1024.map",...
    "street-map/Berlin_2_256.map", "street-map/Berlin_2_512.map", "street-map/Berlin_2_1024.map"];
% maps = [
%     "street-map/Berlin_0_256.map", "street-map/Berlin_0_512.map", "street-map/Berlin_0_1024.map",...
%     "street-map/Berlin_1_256.map", "street-map/Berlin_1_512.map", "street-map/Berlin_1_1024.map",...
%     "street-map/Berlin_2_256.map", "street-map/Berlin_2_512.map", "street-map/Berlin_2_1024.map",...
%     "street-map/Boston_0_256.map", "street-map/Boston_0_512.map", "street-map/Boston_0_1024.map",...
%     "street-map/Boston_1_256.map", "street-map/Boston_1_512.map", "street-map/Boston_1_1024.map",...
%     "street-map/Boston_2_256.map", "street-map/Boston_2_512.map", "street-map/Boston_2_1024.map",...
%     "street-map/Denver_0_256.map", "street-map/Denver_0_512.map", "street-map/Denver_0_1024.map",...
%     "street-map/Denver_1_256.map", "street-map/Denver_1_512.map", "street-map/Denver_1_1024.map",...
%     "street-map/Denver_2_256.map", "street-map/Denver_2_512.map", "street-map/Denver_2_1024.map",...
%     "street-map/London_0_256.map", "street-map/London_0_512.map", "street-map/London_0_1024.map",...
%     "street-map/London_1_256.map", "street-map/London_1_512.map", "street-map/London_1_1024.map",...
%     "street-map/London_2_256.map", "street-map/London_2_512.map", "street-map/London_2_1024.map",...
%     "street-map/Milan_0_256.map", "street-map/Milan_0_512.map", "street-map/Milan_0_1024.map",...
%     "street-map/Milan_1_256.map", "street-map/Milan_1_512.map", "street-map/Milan_1_1024.map",...
%     "street-map/Milan_2_256.map", "street-map/Milan_2_512.map", "street-map/Milan_2_1024.map",...
%     "street-map/Moscow_0_256.map", "street-map/Moscow_0_512.map", "street-map/Moscow_0_1024.map",...
%     "street-map/Moscow_1_256.map", "street-map/Moscow_1_512.map", "street-map/Moscow_1_1024.map",...
%     "street-map/Moscow_2_256.map", "street-map/Moscow_2_512.map", "street-map/Moscow_2_1024.map",...
%     "street-map/NewYork_0_256.map", "street-map/NewYork_0_512.map", "street-map/NewYork_0_1024.map",...
%     "street-map/NewYork_1_256.map", "street-map/NewYork_1_512.map", "street-map/NewYork_1_1024.map",...
%     "street-map/NewYork_2_256.map", "street-map/NewYork_2_512.map", "street-map/NewYork_2_1024.map",...
%     "street-map/Paris_0_256.map", "street-map/Paris_0_512.map", "street-map/Paris_0_1024.map",...
%     "street-map/Paris_1_256.map", "street-map/Paris_1_512.map", "street-map/Paris_1_1024.map",...
%     "street-map/Paris_2_256.map", "street-map/Paris_2_512.map", "street-map/Paris_2_1024.map",...
%     "street-map/Shanghai_0_256.map", "street-map/Shanghai_0_512.map", "street-map/Shanghai_0_1024.map",...
%     "street-map/Shanghai_1_256.map", "street-map/Shanghai_1_512.map", "street-map/Shanghai_1_1024.map",...
%     "street-map/Shanghai_2_256.map", "street-map/Shanghai_2_512.map", "street-map/Shanghai_2_1024.map",...
%     "street-map/Sydney_0_256.map", "street-map/Sydney_0_512.map", "street-map/Sydney_0_1024.map",...
%     "street-map/Sydney_1_256.map", "street-map/Sydney_1_512.map", "street-map/Sydney_1_1024.map",...
%     "street-map/Sydney_2_256.map", "street-map/Sydney_2_512.map", "street-map/Sydney_2_1024.map"];

times = zeros(1,length(maps));
iterations = 10;
for it = 1:iterations
    for i = 1:length(maps)
        binMap = loadMap(maps(i));
        tic
        adjacencies(binMap);
        nclass1 = equiClass();
        times(i) = times(i) + 1/iterations * toc;
    end
end
times

% dla wszystkich:
% times =
% 
%   Columns 1 through 12
% 
%     0.2644    1.0362    4.1379    0.2821    1.0883    4.0141    0.2962    1.1100    4.0648    0.2729    1.0834    4.0050
% 
%   Columns 13 through 24
% 
%     0.2756    1.0822    4.1108    0.2878    1.1473    4.2568    0.2812    1.1118    4.1865    0.2868    1.0935    4.1366
% 
%   Columns 25 through 36
% 
%     0.2791    1.0918    4.1191    0.2807    1.0818    4.1344    0.2766    1.0837    4.0833    0.2756    1.0530    4.0102
% 
%   Columns 37 through 48
% 
%     0.2848    1.0716    4.0763    0.2843    1.0940    4.0243    0.2781    1.0833    4.1622    0.2672    1.0656    4.1019
% 
%   Columns 49 through 60
% 
%     0.2781    1.0908    4.0755    0.2716    1.0621    4.0261    0.2787    1.0887    4.2518    0.2774    1.0660    4.1662
% 
%   Columns 61 through 72
% 
%     0.2837    1.1138    4.1775    0.2801    1.0767    4.1067    0.2880    1.0816    4.0195    0.2795    1.0758    4.1011
% 
%   Columns 73 through 84
% 
%     0.2775    1.0896    4.2216    0.2783    1.0800    4.0586    0.2657    1.0677    4.0692    0.2807    1.1030    4.1624
% 
%   Columns 85 through 90
% 
%     0.2741    1.0794    4.1023    0.2747    1.0927    4.0873