function [] = updatePair(p, q)
%UPDATATEPAIR Aktualizuje relację przylegania dla pojedynczej
%             pary wierzchołków.
%    Wejście
%    p, q       indeksy wierzchołków w macierzy uglobal
%    uglobal    globalna macierz relacji przylegania
%
%    Wyjście
%    uglobal    zmodyfikowana globalna macierz relacji 
%               przylegania
    global uglobal
    % wyszukanie korzeni drzew dla wierzchołków
    rp = getRootOf(p);
    rq = getRootOf(q);
    % jeśli drzewa są różne, to je łączymy
    if (rp ~= rq)
        % gdy drzewo z korzeniem rp jest większe niż drzewo 
        % z korzeniem rq
        if (-1)*uglobal(rp) < (-1)*uglobal(rq) 
            %! ponieważ wartości w korzeniach są ujemne
            % aktualizacja rozmiaru większego drzewa
            uglobal(rq) = uglobal(rp) + uglobal(rq);
            % inteligentne łączenie korzenia rp (mniejszego drzewa)
            % z korzeniem rq (większego drzewa) 
            uglobal(rp) = rq;
            % łączenie wierzchołków poniżej wierzchołka p i q 
            % bezpośrednio do korzenia rq (przez użycie setValue)
            setValue(p, rq);
            setValue(q, rq);
        % gdy drzewo z korzeniem rq jest większe niż drzewo 
        % z korzeniem rp
        else 
            % aktualizacja rozmiaru większego drzewa
            uglobal(rp) = uglobal(rp) + uglobal(rq);
            % inteligentne łączenie korzenia rq (mniejszego drzewa)
            % z korzeniem rp (większego drzewa)
            uglobal(rq) = rp;
            % łączenie wierzchołków poniżej wierzchołka p i q 
            % bezpośrednio do korzenia rq (przez użycie setValue)
            setValue(p, rp);
            setValue(q, rp);
        end
    % jeśli wierzchołki są z tego samego drzewa
    else
            % łączenie wierzchołków poniżej wierzchołka p i q 
            % bezpośrednio do korzenia rp (przez użycie setValue)
            setValue(p, rp);
            setValue(q, rp);
    end

end
