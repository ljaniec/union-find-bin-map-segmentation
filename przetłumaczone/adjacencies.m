function [] = adjacencies(binMap)
% ADJACENCIES Wyznacza i zapisuje releację przylegania danej
%             mapy binarnej.
%   
%   Na wyjściu każdy korzeń zawiera ilość wierzchołków 
%   w drzewie, przemnożoną przez minus jeden. 
%   Każdy wierzchołek zawiera indeks kolejnego wierzchołka 
%   lub korzenia.
%
%   Wejście
%   binMap      prostokątny obraz złożony z zer i jedynek
%   
%   Wyjście
%   uglobal     globalna macierz relacji przylegania
global uglobal
uglobal = (-1)*binMap;
[h, w] = size(uglobal); % wymiary obrazu
    
% pętle wykrywające i zapisujące relację przylegania
for j = 1:w % pętla po kolumnach
    for i = 1:(h-1)
        if binMap(i,j) && binMap(i+1,j)
            p = (j-1) * h + i; 
% (j-1) * h, nie w, bo "column-first" macierze są w pamięci Matlaba(!)
            q = p + 1;
            updatePair(p, q);
        end
    end
end
    
for j = 1:(w-1) % kolejność jest ważna, to jest bardziej optymalne - cytat z MathWorks
    for i = 1:h % pętla po wierszach
        if binMap(i,j) && binMap(i,j+1) 
        % dwie kolumny, nadal lepiej niż wiersze
            p = (j-1) * h + i;
            q = p + h;
            updatePair(p, q);
        end
    end
end
% ważne uwagi - 
% "column-first" in memory for matrix
% podobnie z mnożeniem A*x, kombinacja liniowa kolumn, współczynniki -
% składowe wektora (wyliczanie całych kolumn i ich suma na końcu), 
% a w C - inny algorytm jest (wyliczanie po kolei x1, x2, itd. z x)
%------------------------- for diagonals
% top left to down right
% top right to down left



