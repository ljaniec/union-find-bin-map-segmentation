function [] = adjacencies(binMap)
% ADJACENCIES Determine and record adjacencies of ones in the binary map 
%   On exit, each root contains the negative of the number of
%   elements in its tree. Each leaf contains the index of the
%   next leaf or its root.

%   Input
%   binMap - a rectangular mask, ie. binary map of 0's and 1's,
%   
%   Output
%   uglobal - a global array recording the adjacencies in the map
    global uglobal
    %uglobal = (-1) * ones(1,sum(binMap, 'all')); % Number of sets is at 
                                        % most the number of 1's in the map
    uglobal = (-1)*binMap;

    % dimensions of map
    [h, w] = size(uglobal);
    
    % cross loops recording adjacencies
    for j = 1:w % column loop
        for i = 1:(h-1)
            if binMap(i,j) && binMap(i+1,j)
                p = (j-1) * h + i; % (j-1) * h, nie w, bo "column-first" macierze są w pamięci Matlaba(!)
                q = p + 1;
                updatePair(p, q);
            end
        end
    end
    
    for j = 1:(w-1) % kolejność jest ważna, to jest bardziej optymalne - cytat z MathWorks
        for i = 1:h % row loop
            if binMap(i,j) && binMap(i,j+1) % dwie kolumny, nadal lepiej niż wiersze
                p = (j-1) * h + i;
                q = p + h;
                updatePair(p, q);
            end
        end
    end
    % ważne uwagi - 
    % "column-first" in memory for matrix
    % podobnie z mnożeniem A*x, kombinacja liniowa kolumn, współczynniki -
    % składowe wektora (wyliczanie całych kolumn i ich suma na końcu), 
    % a w C - inny algorytm jest (wyliczanie po kolei x1, x2, itd. z x)
    %------------------------- for diagonals
    % top left to down right
    % top right to down left
end


