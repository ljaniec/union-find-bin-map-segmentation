function [] = printMap(name)
%PRINTMAP Display and save given map under given name
%   Input
%   uglobal     a global array recording the relations, after a call to 
%               adjacencies()
%   name        name for file, i.e. 'map1.png'
%   
global uglobal
imagesc(uglobal)
colormap(jet)
set(gca,'xtick',[])
set(gca,'ytick',[])
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
saveas(gcf, name)
end

