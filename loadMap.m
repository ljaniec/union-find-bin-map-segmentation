function [binaryMap] = loadMap(map)
% LOADMAP Load given map in a form given in the Sturtevant's set of map.
%   binaryM = loadMap(ExMap) load map of @ and . as binary Map
%%% See also:
% @article{sturtevant2012benchmarks,
%   title={Benchmarks for Grid-Based Pathfinding},
%   author={Sturtevant, N.},
%   journal={Transactions on Computational Intelligence and AI in Games},
%   volume={4},
%   number={2},
%   pages={144 -- 148},
%   year={2012},
%   url = {http://web.cs.du.edu/~sturtevant/papers/benchmarks.pdf},
% }
%%% Legend for map:
% . - passable terrain
% G - passable terrain
% @ - out of bounds
% O - out of bounds
% T - trees (unpassable)
% S - swamp (passable from regular terrain) -> passable
% W - water (traversable, but not passable from terrain) -> unpassable

fileID = fopen(map, 'r');
fgetl(fileID); % ignore 'type octile'
height = sscanf(fgetl(fileID), 'height %d'); % columns numbered from 0
width = sscanf(fgetl(fileID), 'width %d'); % columns numbered from 0
fgetl(fileID); % ignore 'map'
passable = ['.', 'G', 'S'];
%unpassable = ['@', 'O', 'T', 'W'];
binaryMap = zeros(height, width);
for i = 1:height
    tmpLine = fgetl(fileID);
    for j = 1:width
        tmpChar = tmpLine(j);
        if strcmp(tmpChar, passable(1))||...
           strcmp(tmpChar, passable(2))||...
           strcmp(tmpChar, passable(3))
            binaryMap(i,j) = 0;
        else
            binaryMap(i,j) = 1;
        end
    end
end
fclose(fileID);
end
